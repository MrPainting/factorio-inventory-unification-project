## Name
The Factorio Inventory Unification Project - FIUP - (Name is Subject to change)

## Description
Factorio's base-game Inventory UI is... Fine. It is "Functional". 
Factorio's Modded Inventory UI is... Less Good. It's bad. Terrible, really. The issue comes down to the tendancy of mod authors to create new tabs for every catagory of thing that they add to the game, even when that tab only contains 5 things. 

This mod aims to fix that, primarily by applying a few simple rules, on top of which all of the items in all of the mods across the entire community can be organized. 

The Goal is simple: Never have to ask, "What tab is <Misc. Item X> in?", ever again. 

The UI has tabs. Those tabs have pictures on them. It is not hard to imagine a system which solves this problem. Honestly I am surprised a community standard hasnt arisen already. 

## Installation
1. Download Archive from the RELEASES page
2. Put in Mod Folder - Typically in "C:\Users\<USRNAME>\AppData\Roaming\Factorio\mods"
3. ???
4. Profit.

## Roadmap
TBA

## Contributing
TBA

## Authors and acknowledgment
Spiritual Successor to "Sandro's Fixes", to whom I credit the code format.

## License
LICENSE: [MIT](https://opensource.org/licenses/MIT)

