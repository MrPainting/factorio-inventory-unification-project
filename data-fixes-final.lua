local armor = data.raw.armor
local ammo = data.raw.ammo
local item = data.raw.item
local fluid = data.raw.fluid
local lab = data.raw.lab
local recipe = data.raw.recipe

local subgroup = data.raw["item-subgroup"]
local item_with_enitity_data = data.raw["item-with-entity-data"]

local function sort_recipe(recipe_sort, subgroup)
  if recipe[recipe_sort] then
    recipe[recipe_sort].subgroup = subgroup
  else
    log("Recipe " .. recipe_sort .. " does not exist.")
  end
end

local function sort_item_recipe(item_sort, subgroup)
  if item[item_sort] then
    item[item_sort].subgroup = subgroup
  else
    log("Item " .. item_sort .. " does not exist.")
  end
  if recipe[item_sort] then
    recipe[item_sort].subgroup = subgroup
  else
    log("Recipe " .. item_sort .. " does not exist.")
  end
end

local function sort_item_recipe_order(item_sort, subgroup, order)
  local any

  if item[item_sort] then
    any = item
  elseif ammo[item_sort] then
    any = ammo
  elseif armor[item_sort] then
    any = armor
  elseif item_with_enitity_data[item_sort] then
    any = item_with_enitity_data
  else
    log("Item " .. item_sort .. " does not exist.")
  end

  if any[item_sort] then
    any[item_sort].subgroup = subgroup
    any[item_sort].order = order
  end

  if recipe[item_sort] then
    recipe[item_sort].subgroup = subgroup
    recipe[item_sort].order = order
  else
    log("Recipe " .. item_sort .. " does not exist.")
  end
end
